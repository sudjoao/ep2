/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import model.Pokemon;
import model.Treinador;

/**
 *
 * @author sudjoao
 */
public class InterfaceCadastroTreinador extends javax.swing.JFrame {

    private Integer idInicial;

    /**
     * Creates new form InterfaceCadastroTreinador
     */
    public InterfaceCadastroTreinador() {
        initComponents();
        bgSexo.add(rbF);
        bgSexo.add(rbM);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgSexo = new javax.swing.ButtonGroup();
        lLogo = new javax.swing.JLabel();
        lNome = new javax.swing.JLabel();
        lSexo = new javax.swing.JLabel();
        lRegiao = new javax.swing.JLabel();
        tNome = new javax.swing.JTextField();
        tRegiao = new javax.swing.JTextField();
        bCadastrar = new javax.swing.JButton();
        lStatus = new javax.swing.JLabel();
        rbM = new javax.swing.JRadioButton();
        rbF = new javax.swing.JRadioButton();
        lPokemonInicial = new javax.swing.JLabel();
        tPokemonInicial = new javax.swing.JTextField();
        lPokemonInvalido = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(800, 600));
        setResizable(false);
        setSize(new java.awt.Dimension(600, 400));

        lLogo.setFont(new java.awt.Font("Ubuntu", 0, 48)); // NOI18N
        lLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Pokedex.png"))); // NOI18N

        lNome.setText("Digite o nome do treinador:");

        lSexo.setText("Selecione seu sexo:");

        lRegiao.setText("Digite a região inicial do treinador:");

        bCadastrar.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        bCadastrar.setText("Cadastrar");
        bCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCadastrarActionPerformed(evt);
            }
        });

        lStatus.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N

        rbM.setText("M");
        rbM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbMActionPerformed(evt);
            }
        });

        rbF.setText("F");

        lPokemonInicial.setText("Digite o pokémon inicial:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(220, 220, 220)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lLogo)
                            .addComponent(lStatus)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lRegiao)
                                    .addComponent(lSexo)
                                    .addComponent(lNome)
                                    .addComponent(lPokemonInicial))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tPokemonInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(tNome, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(rbM)
                                        .addGap(18, 18, 18)
                                        .addComponent(rbF))
                                    .addComponent(tRegiao, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lPokemonInvalido)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(307, 307, 307)
                        .addComponent(bCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(19, 19, 19))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lLogo)
                .addGap(173, 173, 173)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lNome)
                    .addComponent(tNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lSexo)
                    .addComponent(rbM)
                    .addComponent(rbF))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lRegiao)
                    .addComponent(tRegiao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lPokemonInicial)
                    .addComponent(tPokemonInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lStatus)
                .addGap(26, 26, 26)
                .addComponent(lPokemonInvalido)
                .addGap(7, 7, 7)
                .addComponent(bCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public Boolean testarPokemon(String nome) {
        for (int i = 0; i < 721; i++) {
            if (nome.toLowerCase().equals(InterfacePrincipal.pokemons[i].getNome().toLowerCase())) {
                idInicial = i;
                return true;
            }
        }
        return false;
    }
    private void bCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCadastrarActionPerformed
        // TODO add your handling code here:
        if (!testarPokemon(tPokemonInicial.getText())) {
            lPokemonInvalido.setText("Nome de pokémon invalido");
        } else {
            if (tNome.getText().trim().equals("") || tRegiao.getText().trim().equals("") || (!rbF.isSelected() && !rbM.isSelected())) {
                lStatus.setText("Algum campo está vazio escreva o dado para completar o cadastro");
            } else {
                String sex;
                if (rbF.isSelected()) {
                    sex = "F";
                } else {
                    sex = "M";
                }
                InterfacePrincipal.treinadores[InterfacePrincipal.numeroTreinadores] = new Treinador(tNome.getText(), tRegiao.getText(), sex, (Pokemon) InterfacePrincipal.pokemons[idInicial]);
                InterfacePrincipal.numeroTreinadores++;
                tNome.setText(" ");
                tRegiao.setText(" ");
                rbF.setSelected(false);
                rbM.setSelected(false);
                lStatus.setText("Cadastrado com sucesso, você já pode fechar essa aba");
                lPokemonInvalido.setText("");

            }
        }
    }//GEN-LAST:event_bCadastrarActionPerformed

    private void rbMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbMActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbMActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bCadastrar;
    private javax.swing.ButtonGroup bgSexo;
    private javax.swing.JLabel lLogo;
    private javax.swing.JLabel lNome;
    private javax.swing.JLabel lPokemonInicial;
    private javax.swing.JLabel lPokemonInvalido;
    private javax.swing.JLabel lRegiao;
    private javax.swing.JLabel lSexo;
    private javax.swing.JLabel lStatus;
    private javax.swing.JRadioButton rbF;
    private javax.swing.JRadioButton rbM;
    private javax.swing.JTextField tNome;
    private javax.swing.JTextField tPokemonInicial;
    private javax.swing.JTextField tRegiao;
    // End of variables declaration//GEN-END:variables
}
