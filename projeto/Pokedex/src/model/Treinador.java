/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author sudjoao
 */
public class Treinador {

    private String nome;
    private String sexo;
    private String regiao;
    public Pokemon[] pokemons = new Pokemon[721];
    public int nPokemons = 1;

    public Treinador() {
    }

    public Treinador(String nome, String regiao, String sexo, Pokemon pokemonInicial) {
        for (int i = 0; i < 721; i++) {
            pokemons[i] = new Pokemon();
        }
        this.nome = nome;
        this.regiao = regiao;
        this.sexo = sexo;
        pokemons[0] = pokemonInicial;
    }

    public String getNome() {
        return nome;
    }

    public String getSexo() {
        return sexo;
    }

    public String getRegiao() {
        return regiao;
    }

    public String getIconPokemon(int i) {
        return pokemons[i].getIcon();
    }

    public Pokemon[] getPokemons() {
        return pokemons;
    }

    public void setPokemon(Pokemon pokemon, int i) {
        pokemons[i] = pokemon;
    }

}
