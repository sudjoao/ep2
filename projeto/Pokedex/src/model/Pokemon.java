/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author sudjoao
 */
public class Pokemon {

    private int id;
    private String nome;
    private String tipo1;
    private String tipo2;
    private String abilitie1;
    private String abilitie2;
    private String abilitie3;
    private int geracao;
    private String icon;

    public static void lerCsv(Pokemon[] pokemon) {
        String arquivoCSV = "data/csv_files/POKEMONS_DATA_1.csv";
        BufferedReader buff = null;
        String linha;
        int i = -1;
        String csvDivisor = ",";
        try {
            buff = new BufferedReader(new FileReader(arquivoCSV));
            while ((linha = buff.readLine()) != null) {
                String[] dados = linha.split(csvDivisor);
                if (i >= 0) {
                    pokemon[i] = new Pokemon();
                    pokemon[i].id = Integer.parseInt(dados[0]);
                    pokemon[i].nome = dados[1];
                    pokemon[i].tipo1 = dados[2];
                    pokemon[i].tipo2 = dados[3];
                    pokemon[i].geracao = Integer.parseInt(dados[11]);
                    pokemon[i].icon = "/images/" + pokemon[i].nome.toLowerCase() + ".png";
                }
                i++;
            }
            arquivoCSV = "data/csv_files/POKEMONS_DATA_2.csv";
            i = -1;
            buff = new BufferedReader(new FileReader(arquivoCSV));
            while ((linha = buff.readLine()) != null) {
                String[] dados = linha.split(csvDivisor);
                if (i >= 0) {
                    pokemon[i].abilitie1 = dados[5];
                    pokemon[i].abilitie2 = dados[6];
                    pokemon[i].abilitie3 = dados[7];

                }
                i++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (buff != null) {
                try {
                    buff.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getIcon() {
        return icon;
    }

    public String getAbilitie1() {
        return abilitie1;
    }

    public String getAbilitie2() {
        return abilitie2;
    }

    public String getAbilitie3() {
        return abilitie3;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getTipo1() {
        return tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public int getGeracao() {
        return geracao;
    }

    public Pokemon() {
        id = 0;
        nome = " ";
        tipo1 = " ";
        tipo2 = " ";
        geracao = 0;
    }

    public void apagar() {
        nome = "Teste";
        abilitie1 = " ";
        abilitie2 = " ";
        abilitie3 = " ";
        geracao = 0;
        icon = " ";
        tipo1 = " ";
        tipo2 = " ";
        id = 0;
    }



}
