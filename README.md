# Exercício Programado 2

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps/eps_2018_2/ep2/wikis/home).

## Como usar o projeto
```sh
 * Projeto feito na interface NetBeans 8.2
```

## Funcionalidades do projeto
```sh
 * Consulta Pokémon você pode consultar os pokémons por tipo/ nome, os nomes tem que ser completos seguindo o padrão do arquivo CSV fornecido
 * Ao clicar em um nome de pokémon listado você pode acessar suas informações
 * Você pode fechar as telas secundárias(Cadastro, Consulta e Alterar dados) para voltar para tela principal
 * Na tela cadastro você cadastra um novo treinador
 * Na tela Consulta Treinador você pode consultar todos os dados de um treinador selecionado e de seus pokémons
 * Na tela Altera Dados você pode cadastrar mais pokémons a cada treinador
```

## Bugs e Problemas
```sh
 * Talvez pode ter acontecido de alguma imagem ter ficado com o nome diferente do pokémon no csv e ao tentar abrir esse pokémon não abra pois não vai encontrar a imagem que o representa
```

## Referencias
```sh
 * Imagens e CSV fornecidas na wiki do trabalho
 * Nintendo por esse jogo maravilhoso que é Pokémon
 * StackOverflox por alguns tutorias que ajudaram bastante
```
